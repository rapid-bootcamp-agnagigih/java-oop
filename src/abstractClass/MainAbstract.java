package abstractClass;

public class MainAbstract {
    public static void main(String[] args) {
        //Location location = new Location();
        City city = new City();
        city.name = "Bandung";
        System.out.println("City's name : " + city.name);
    }
}
