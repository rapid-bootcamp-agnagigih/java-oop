public class MainProduct {
    public static void main(String[] args) {
        Product product1 = new Product("Nasi Goreng", 15_000);
        product1.showPrice();

        Product product2 = new Product(2, "Nasi Ayam", 25_000);
        product2.showPrice();

        Product product3 = new Product(3, "Nasi Rendang", 30_000);
        product3.showPrice();
    }
}
