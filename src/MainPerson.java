public class MainPerson {
    public static void main(String[] args) {
        // class - object - intance - constructor
        Person person1 = new Person();
        person1.id = 1;
        person1.name = "orang1";
        person1.address = "alamat1";
        person1.age = 23;
        person1.gender = "Pria";
        person1.sayHello();

        Person person2 = new Person();
        person2.id = 2;
        person2.name = "orang2";
        person2.address = "alamat2";
        person2.age = 21;
        person2.gender = "Wanita";
        person2.sayHello();

        Person person3 = new Person();
        person3.id = 3;
        person3.name = "orang3";
        person3.address = "alamat3";
        person3.age = 25;
        person3.gender = "Pria";
        person3.sayHello();

        Person person4 = new Person();
        person4.id = 4;
        person4.name = "orang4";
        person4.address = "alamat4";
        person4.age = 22;
        person4.gender = "Wanita";
        person4.sayHello();
    }
}
