package abstractMethod;

public class PolarBear extends Animal{
    public PolarBear(String name) {
        this.name = name;
    }

    @Override
    void run() {
        System.out.println("The polar bear's name is "+ name + ", can run.");
    }

    @Override
    void eat() {
        System.out.println("The polar bear's name is "+ name + ", can eat.");
    }
}
