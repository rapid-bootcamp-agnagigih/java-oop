package abstractMethod;

public class Goat extends Animal{
    public Goat(String name){
        this.name = name;
    }

    @Override
    void run() {
        System.out.println("The goat's name is "+ name + ", can run.");
    }

    @Override
    void eat() {
        System.out.println("The goat's name is "+ name + ", can eat.");
    }
}
