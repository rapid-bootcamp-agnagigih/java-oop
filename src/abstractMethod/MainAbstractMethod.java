package abstractMethod;

public class MainAbstractMethod {
    public static void main(String[] args) {
        Cat cat = new Cat("Rosmontis");
        cat.run();
        cat.eat();

        Rabbit rabbit = new Rabbit("Amiya");
        rabbit.run();
        rabbit.eat();

        Fox fox = new Fox("Angelina");
        fox.run();
        fox.eat();

        PolarBear polarBear = new PolarBear("Rosa");
        polarBear.run();
        polarBear.eat();

        Dog dog = new Dog("Podenco");
        dog.run();
        dog.eat();

        Goat goat = new Goat("Eyjafjalla");
        goat.run();
        goat.eat();
    }
}
