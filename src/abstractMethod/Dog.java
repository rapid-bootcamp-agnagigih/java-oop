package abstractMethod;

public class Dog extends Animal{
    public Dog(String name){
        this.name = name;
    }

    @Override
    void run() {
        System.out.println("The dog's name is "+ name + ", can run.");
    }

    @Override
    void eat() {
        System.out.println("The dog's name is "+ name + ", can eat.");
    }
}
