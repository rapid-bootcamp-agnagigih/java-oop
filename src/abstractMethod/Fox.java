package abstractMethod;

public class Fox extends Animal{
    public Fox(String name) {
        this.name = name;
    }

    @Override
    void run() {
        System.out.println("The fox's name is " + name + ", can run.");
    }

    @Override
    void eat() {
        System.out.println("The fox's name is " + name + ", can eat.");
    }
}
