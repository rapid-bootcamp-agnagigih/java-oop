package abstractMethod;

public class Cat extends Animal{
    public Cat(String name) {
        this.name = name;
    }

    @Override
    void run() {
        System.out.println("The cat's name is "+ name + ", can run.");
    }

    @Override
    void eat() {
        System.out.println("The cat's name is " + name + ", can eat.");
    }
}
