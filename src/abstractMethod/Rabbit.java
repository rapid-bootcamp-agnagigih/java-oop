package abstractMethod;

public class Rabbit extends Animal{
    public Rabbit(String name) {
        this.name = name;
    }

    @Override
    void run() {
        System.out.println("The rabbit's name is " + name +", can run.");
    }

    @Override
    void eat() {
        System.out.println("The rabbit's name is " + name + ", can eat.");
    }
}
